package com.varshith.myapplication

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.sql.Timestamp

@Entity(tableName = "notesTable")
data class Note (
     @ColumnInfo(name = "Title")val noteTitle:String,
    @ColumnInfo(name = "description")val noteDescription:String,
     @ColumnInfo(name = "timestamp")val timestamp: String
        )
{


    @PrimaryKey(autoGenerate = true)
    var id=0

}