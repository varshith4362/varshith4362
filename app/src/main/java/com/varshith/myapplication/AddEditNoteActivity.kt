package com.varshith.myapplication

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.net.Uri
import android.os.Build
import android.os.Build.VERSION
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.varshith.myapplication.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_add_edit_note.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class AddEditNoteActivity : AppCompatActivity() {
    lateinit var viewModal: NoteViewModel
    lateinit var noteTitleEdt: EditText
    lateinit var noteDescriptionEdt: EditText
    lateinit var addUpdateBtn: Button
    lateinit var main_layout:LinearLayout

    lateinit var bitmap: Bitmap
    var bodf=true
    val REQUEST_PERM_WRITE_STORAGE = 102
    private val CAPTURE_PHOTO = 104
    internal var imagePath: String? = ""
    var noteId = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit_note)

        noteTitleEdt = idEdtNoteName
        noteDescriptionEdt = idEdtNoteDesc
        addUpdateBtn = idBtn
        val textsharing = share_text

        viewModal = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        ).get(NoteViewModel::class.java)
        val noteType = intent.getStringExtra("noteType")
        if (noteType.equals("Edit")) {
            val noteTitle = intent.getStringExtra("noteTitle")
            val noteDescription = intent.getStringExtra("noteDescription")
            noteId = intent.getIntExtra("noteId", -1)
            addUpdateBtn.setText("Update")
            noteTitleEdt.setText(noteTitle)
            noteDescriptionEdt.setText(noteDescription)

        } else {
            textsharing.visibility = View.INVISIBLE
            share_img.visibility=View.INVISIBLE
            addUpdateBtn.setText("Save")
        }
        addUpdateBtn.setOnClickListener {
            val noteTitle = noteTitleEdt.text.toString()
            val noteDescription = noteDescriptionEdt.text.toString()
            if (noteType.equals("Edit")) {
                if (noteTitle.isNotEmpty() && noteDescription.isNotEmpty()) {
                    val sdf = SimpleDateFormat("dd /MMM/yyyy - HH:mm")
                    val currentDateAndTime: String = sdf.format(Date())
                    val updatedNote = Note(noteTitle, noteDescription, currentDateAndTime)
                    updatedNote.id = noteId
                    viewModal.updateNote(updatedNote)
                    bodf=false


                }
            } else {

                if (noteTitle.isNotEmpty() && noteDescription.isNotEmpty()) {
                    val sdf = SimpleDateFormat("dd/MM/yyyy - HH:mm")
                    val currentDateAndTime: String = sdf.format(Date())
                    viewModal.addNote(Note(noteTitle, noteDescription, currentDateAndTime))



                }
            }
            val intent=Intent(applicationContext, MainActivity::class.java)
            var msg="updated"
            if(bodf)
            {
                msg="created"
            }
            intent.putExtra("about",msg)
            intent.putExtra("values",noteTitle)
            startActivity(intent)
            this.finish()
        }

        textsharing.setOnClickListener {
            val noteTitle = noteTitleEdt.text.toString()
            val noteDescription = noteDescriptionEdt.text.toString()
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, "Title: $noteTitle || Description : $noteDescription")
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)

        }
        share_img.setOnClickListener {
            val file=takeScreenshot()
            if(file!=null)
            {
                share(file)
            }


        }


        }

    private fun share(file: File) {
        val uri=FileProvider.getUriForFile(Objects.requireNonNull(getApplicationContext()),
            BuildConfig.APPLICATION_ID + ".provider", file)
        val intent=Intent()
        intent.action=Intent.ACTION_SEND
        intent.putExtra(Intent.EXTRA_STREAM,uri)
        intent.type="image/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(Intent.createChooser(intent, null))



    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if(grantResults.size>0 and grantResults[0].compareTo(PackageManager.PERMISSION_GRANTED))
        {
            share_img.performClick()
        }
        else{
            Toast.makeText(this,"Permission Denied",Toast.LENGTH_LONG).show()
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    @SuppressLint("SimpleDateFormat")
    private fun takeScreenshot():File? {

        if(!checkCondition())
            return null
        try {
            val path=Environment.getExternalStorageDirectory().toString()+"/AppName"
            val fileDir:File=File(path)
            val fileExists = fileDir.exists()
            if(!fileExists)
            {
                fileDir.mkdir()


            }
            val mpath=path+"Screenshot_"+Date().time+".png"
            val bitmap:Bitmap=screenShot()
            val file:File=File(mpath)
            val fout=  FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG,100,fout)
            fout.flush()
            fout.close()
            println("Working")
            Toast.makeText(this,"File saved succesfully",Toast.LENGTH_LONG).show()
            return file

        }
        catch (e:FileNotFoundException)
        {
            e.printStackTrace()
            println("errors")
        }
        catch (e:IOException)
        {
            e.printStackTrace()
        }
        return null





    }

    private fun screenShot(): Bitmap {
        val views:View=relative_layout
        val bitmap:Bitmap= Bitmap.createBitmap(views.width,views.height,Bitmap.Config.ARGB_8888)
        val canvas=Canvas(bitmap)
        views.draw(canvas)
        return bitmap
    }

    private fun checkCondition(): Boolean {
        val permis=ActivityCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permis != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    1
                )
                return false
            }
        }
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this@AddEditNoteActivity, MainActivity::class.java))
        finish()
    }

}