package com.varshith.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_add_edit_note.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), NoteClickDeleteInterface, NoteClickInterface,androidx.appcompat.widget.SearchView.OnQueryTextListener {
    lateinit var viewModal: NoteViewModel
    lateinit var notesRV: RecyclerView
    lateinit var addFAB: FloatingActionButton
    lateinit var noteRVAdapter:NoteRVAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        notesRV=idRVNotes
        addFAB=idFABAddNote
        notesRV.layoutManager=LinearLayoutManager(this)
         noteRVAdapter=NoteRVAdapter(this,this, this)
        notesRV.adapter=noteRVAdapter
        viewModal=ViewModelProvider(this,ViewModelProvider.AndroidViewModelFactory.getInstance(application)).get(NoteViewModel::class.java)
        viewModal.allNotes.observe(this, Observer { list ->
            list?.let {
                // on below line we are updating our list.
                noteRVAdapter.updateList(it)
            }
        })
        addFAB.setOnClickListener {
            val intent= Intent(this@MainActivity, AddEditNoteActivity::class.java)
            startActivity(intent)
            this.finish()
        }
        val msgs=intent.getStringExtra("about")
        println(msgs)

    }


    override fun onDeleteIconClick(note: Note) {
        val builder=AlertDialog.Builder(this)
        builder.setTitle("Confirm delete")
        builder.setMessage("Are you sure you want to delete this notes")
        builder.setPositiveButton("Yes"){dialogInterface, which ->
            viewModal.deleteNote(note)
            Toast.makeText(this,"${note.noteTitle} deleted",Toast.LENGTH_LONG).show()

        }
        builder.setNeutralButton("Cancel"){dialogInterface , which ->
            Toast.makeText(applicationContext,"clicked cancel\n operation cancel",Toast.LENGTH_LONG).show()
        }
        builder.setNegativeButton("No"){dialogInterface, which ->
            Toast.makeText(applicationContext,"Stopped the deletion",Toast.LENGTH_LONG).show()
        }
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()




    }

    override fun onNoteClick(note: Note) {
        val intent= Intent(this@MainActivity, AddEditNoteActivity::class.java)
        intent.putExtra("noteType", "Edit")
        intent.putExtra("noteTitle", note.noteTitle)
        intent.putExtra("noteDescription", note.noteDescription)
        intent.putExtra("noteId", note.id)
        startActivity(intent)
        this.finish()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu,menu)
        val search= menu.findItem(R.id.menu_search)
        val searchView=search?.actionView as? androidx.appcompat.widget.SearchView
        searchView?.isSubmitButtonEnabled=true
        searchView?.setOnQueryTextListener(this)
        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return true
    }

    override fun onQueryTextChange(query: String?): Boolean {
        if(query != null){
            searchDataBase(query)
        }
        return true
    }
    private fun searchDataBase(query: String)
    {
        val searchQuery = "%$query%"
        viewModal.searchDatabase(searchQuery).observe(this, { list ->
            list.let {
                noteRVAdapter.setData(it)
            }
        })

        }
    }
