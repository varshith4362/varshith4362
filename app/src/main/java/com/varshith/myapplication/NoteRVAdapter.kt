package com.varshith.myapplication

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.note_rv_item.view.*

class NoteRVAdapter(var context:Context,
                    val noteClickDeleteInterface: NoteClickDeleteInterface
                    ,val noteClickInterface: NoteClickInterface):RecyclerView.Adapter<NoteRVAdapter.ViewHolder>()
{
    private var allNotes = ArrayList<Note>()
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // on below line we are creating an initializing all our
        // variables which we have added in layout file.
        val noteTV = itemView.idTVNote
        val dateTV = itemView.idTVDate
        val deleteIV = itemView.idIVDelete
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteRVAdapter.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.note_rv_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return allNotes.size
    }

    override fun onBindViewHolder(holder: NoteRVAdapter.ViewHolder, position: Int) {
        holder.noteTV.setText(allNotes.get(position).noteTitle)
        holder.dateTV.setText("Last updates:"+allNotes.get(position).timestamp)
        holder.deleteIV.setOnClickListener {
            noteClickDeleteInterface.onDeleteIconClick(allNotes.get(position))
        }
        holder.itemView.setOnClickListener {

            noteClickInterface.onNoteClick(allNotes.get(position))
        }
    }
    fun updateList(newList:List<Note>)
    {
        allNotes.clear()
        allNotes.addAll(newList)
        notifyDataSetChanged()
    }
    fun setData(newData: List<Note>){
        allNotes.clear()
        allNotes.addAll(newData)
        notifyDataSetChanged()
    }

}
interface NoteClickDeleteInterface{
    fun onDeleteIconClick(note:Note)

}
interface NoteClickInterface
{
    fun onNoteClick(note: Note)

}
